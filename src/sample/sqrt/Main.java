package sample.sqrt;



public class Main {

    private static Number number = new Number(0);

    /**
     * @param args
     */
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.start();

        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (number.set(i).bingo()) {
                timer.stop();
                System.out.println("Bingo!! " + i + ":" + Math.sqrt(i));
                return;
            }
        }

        timer.stop();
    }

}
