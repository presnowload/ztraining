package codeiq.salvageon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class SalvageonAPI {

    public class Value implements Comparable<Value> {
        public int db;
        public BigInteger index;
        public BigInteger key;
        public BigInteger value;
        public BigInteger maxIndex;

        public Value() {

        }

        public Value(String key) {
            set(2, key);
        }

        public void set(int i, String v) {
            switch (i) {
                case 0:
                    db = Integer.parseInt(v);
                    break;
                case 1:
                    index = new BigInteger(v);
                    break;
                case 2:
                    key = new BigInteger(new StringBuilder(v).delete(0, 1).toString());
                    break;
                case 3:
                    value = new BigInteger(new StringBuilder(v).delete(0, 1).toString());
                    ;
                    break;
                case 4:
                    maxIndex = new BigInteger(v);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        @Override
        public int compareTo(Value other) {
            return new CompareToBuilder()
                .append(this.key, other.key).toComparison();
        }

        @Override
        public boolean equals(Object object) {
            if (!(object instanceof SalvageonAPI.Value))
                return false;
            return EqualsBuilder.reflectionEquals(this.key, ((SalvageonAPI.Value)object).key);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this.key);
        }
    }

    private static void setProxy() {
        System.setProperty("proxySet", "true");
        System.setProperty("proxyHost", "gt2dfr01.2dfacto.local");
        System.setProperty("proxyPort", "8080");
    }

    private static URL createURL(Map<String, BigInteger> requestParams) throws MalformedURLException {
        setProxy();
        StringBuilder builder = new StringBuilder("http://salvageon.textfile.org/");
        builder.append("?");
        for (Map.Entry<String, BigInteger> entry : requestParams.entrySet()) {
            builder.append((String)entry.getKey());
            builder.append("=");
            builder.append((BigInteger)entry.getValue());
            builder.append("&");
        }

        return new URL(builder.toString());
    }

    private static Value getValue(BufferedReader reader) throws IOException {
        String line;
        Value value = new SalvageonAPI().new Value();
        while ((line = reader.readLine()) != null) {
            Pattern pattern = Pattern.compile(" ");
            String[] ss = pattern.split(line, 0);
            int i = 0;
            for (String s : ss) {
                value.set(i++, s);
            }
        }
        return value;

    }

    public Value get(int db, BigInteger index) throws IOException {

        Map<String, BigInteger> requestParams = new HashMap<String, BigInteger>();
        requestParams.put("db", new BigInteger(new Integer(db).toString()));
        requestParams.put("index", index);
        URL url = createURL(requestParams);

        HttpURLConnection connection = null;

        Value value = null;
        try {
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                value = getValue(reader);
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return value;

    }
}
