package codeiq.salvageon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Salvageon {

    private class Value implements Comparable<Value> {
        public int db;
        public BigInteger index;
        public BigInteger key;
        public BigInteger value;
        public BigInteger maxIndex;

        public Value() {

        }

        public Value(String key) {
            set(2, key);
        }

        public void set(int i, String v) {
            switch (i) {
                case 0:
                    db = Integer.parseInt(v);
                    break;
                case 1:
                    index = new BigInteger(v);
                    break;
                case 2:
                    key = new BigInteger(new StringBuilder(v).delete(0, 1).toString());
                    break;
                case 3:
                    value = new BigInteger(new StringBuilder(v).delete(0, 1).toString());
                    ;
                    break;
                case 4:
                    maxIndex = new BigInteger(v);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        @Override
        public int compareTo(Value other) {
            return new CompareToBuilder()
                .append(this.key, other.key).toComparison();
        }

        @Override
        public boolean equals(Object object) {
            if (!(object instanceof Salvageon.Value))
                return false;
            return EqualsBuilder.reflectionEquals(this.key, ((Salvageon.Value)object).key);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this.key);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        db0();
        db1();
        db2();
    }

    private static void db2() throws IOException, InterruptedException {
        System.out.println("===== DB2 =====");
        Value expected = new Salvageon().new Value("K2023636070998557444542586045");
        Value actual = null;
        for (int i = 0; i < 100; i++) {
            BigInteger index = new BigInteger("2").shiftLeft(98);
            index = index.add(new BigInteger("2023636070998557444542586045").shiftRight(1));
            index = index.add(new BigInteger(new Integer(i).toString()));
            actual = executeGet(2, index);

            if (expected.equals(actual))
                break;

            System.out.println(actual);

            sleep();
        }

        System.out.println("Ans." + actual);
    }

    private static void db0() throws IOException, InterruptedException {
        System.out.println("===== DB0 =====");
        for (int i = 0; i < 5; i++) {
            BigInteger index = new BigInteger(new Integer(i).toString());
            Value actual = executeGet(0, index);

            System.out.println(actual);

            sleep();
        }
    }

    private static void db1() throws IOException, InterruptedException {
        System.out.println("===== DB1 =====");
        Value expected = new Salvageon().new Value("K208050656559285601386927895421059705239114932023754");
        Value actual = null;

        BigInteger leftHand = new BigInteger("0");
        BigInteger rightHand = new BigInteger("1267650600228229401496703205375");
        for (int i = 0; i < 100; i++) {
            BigInteger index = leftHand.add(rightHand).shiftRight(1);
            actual = executeGet(1, index);
            //            actual = executeGet(2, index);
            int compareResult = expected.compareTo(actual);
            if (0 == compareResult) {
                break;
            } else if (compareResult < 0) {
                rightHand = index;
            } else {
                leftHand = index;
            }

            System.out.println("[" + i + "]" + "(lhi,rhi,key) = (" + leftHand + "," + rightHand + "," + actual.key + ")");

            sleep();
        }
        System.out.println("Ans." + actual);

    }

    private static void sleep() throws InterruptedException {
        Thread.sleep(1000);
    }

    private static void setProxy() {
        System.setProperty("proxySet", "true");
        System.setProperty("proxyHost", "gt2dfr01.2dfacto.local");
        System.setProperty("proxyPort", "8080");
    }

    private static URL createURL(Map<String, BigInteger> requestParams) throws MalformedURLException {
        setProxy();
        StringBuilder builder = new StringBuilder("http://salvageon.textfile.org/");
        builder.append("?");
        for (Map.Entry<String, BigInteger> entry : requestParams.entrySet()) {
            builder.append((String)entry.getKey());
            builder.append("=");
            builder.append((BigInteger)entry.getValue());
            builder.append("&");
        }

        return new URL(builder.toString());
    }

    private static Value getValue(BufferedReader reader) throws IOException {
        String line;
        Value value = new Salvageon().new Value();
        while ((line = reader.readLine()) != null) {
            Pattern pattern = Pattern.compile(" ");
            String[] ss = pattern.split(line, 0);
            int i = 0;
            for (String s : ss) {
                value.set(i++, s);
            }
        }
        return value;

    }

    private static Value executeGet(int db, BigInteger index) throws IOException {

        Map<String, BigInteger> requestParams = new HashMap<String, BigInteger>();
        requestParams.put("db", new BigInteger(new Integer(db).toString()));
        requestParams.put("index", index);
        URL url = createURL(requestParams);

        HttpURLConnection connection = null;

        Value value = null;
        try {
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                value = getValue(reader);
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return value;

    }
}
