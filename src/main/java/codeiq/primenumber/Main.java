package codeiq.primenumber;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        int number = new Integer(args[0]).intValue();
        List<Integer> primes = new ArrayList<Integer>();
        for (int i = 1; i < number; i++) {
            if (new Number(i).isPrime())
                primes.add(i);
        }

        toString(number, primes);
    }

    private static void toString(int number, List<Integer> primes) {
        System.out.println("[" + number + "] に含まれる素数の数は、[" + primes.size() + "].");
        StringBuilder sb = new StringBuilder("[");
        for (Integer prime : primes) {
            sb.append(prime);
            sb.append(",");
        }
        sb.append("]");
        System.out.println(sb.toString());

    }

}
