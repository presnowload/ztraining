package codeiq.primenumber;

public class Number {
    private final int n;

    Number(int n) {
        this.n = n;
    }

    public boolean isPrime() {
        if (n == 2)
            return true;
        if (n < 2 || n % 2 == 0)
            return false;

        for (int i = 3; i <= Math.sqrt((double)n); i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Integer.toString(n);
    }

}
