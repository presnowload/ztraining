package codeiq.cryptan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		File file = new File(
				"/Users/presnowload/Documents/workspace/ZTraining/src/cryptan/cryptan1.txt");
		FileReader filereader = null;

		Map<String, Integer> m = new HashMap<String, Integer>();
		// ファイル読み込み、空白区切り
		try {
			filereader = new FileReader(file);
			int ch;
			StringBuffer buffer = new StringBuffer();
			while ((ch = filereader.read()) != -1) {

				if (' ' == ch) {
					// System.out.println(",");
					String key = buffer.toString();
					int val = 0;
					if (m.containsKey(key)) {
						val = m.get(key);
						m.put(key, val++);
					} else {
						m.put(key, val);
					}
					buffer.setLength(0);
				} else {
					// System.out.print((char) ch);
					buffer.append((char) ch);
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				filereader.close();
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}

		for (Map.Entry<String, Integer> entry : m.entrySet()) {
			System.out.println("key:" + entry.getKey() + " value:"
					+ entry.getValue());
		}
	}

}
