package codeiq.multiple;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Number {
    private final Integer n;
    private List<Exponentiation> primeFactor;

    public Number(int n) {
        this.n = n;
        primeFactor = new ArrayList<Exponentiation>();
    }

    public boolean isPrime() {
        if (n <= 1)
            return false;
        for (int i = 2; i < n / 2; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    private void decompose() {
        primeFactor.clear();
        int num = n;
        for (int i = 2; i < Math.sqrt(num); i++) {
            // is not prime.
            if (!new Number(i).isPrime())
                continue;

            // is not factor.
            if (num % i != 0)
                continue;

            //            primeFactor.put(i, value)

        }
    }
}
