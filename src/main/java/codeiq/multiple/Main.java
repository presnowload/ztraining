package codeiq.multiple;

/**
 *
 */
public class Main {
    //    最小公倍数が24になるような数の組 (a,b)
    //    a≦b のとき、そのような組は全部で11組
    //    (1,24) (2,24) (3,8) (3,24) (4,24) (6,8)
    //    (6,24) (8,12) (8,24) (12,24) (24,24)
    // 24 = 1 * 2*2*2 * 3
    //    private static final int question = 4;

    // a≦b のとき、最小公倍数が6!（6の階乗、つまり720）となるような組 (a,b) は全部で68組
    // 6！ = 6*5*4*3*2*1 = 1*2*3 * 1*5 * 1*2*2 * 1*3 * 1*2 = 1 * 2^4 * 3^2 * 5

    //        private static final int question = 6;

    // a≦b のとき、最小公倍数が50!（50の階乗）となるような組 (a,b) は全部で何組

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.start();

        final int question;
        if (null == args) {
            question = 50;
        } else {
            question = Integer.valueOf(args[0]);
        }

        LeastCommonMultiple number = new LeastCommonMultiple(question);

        long answer = number.getCount();

        System.out.println("解 = " + answer);
        timer.stop();
    }

}
