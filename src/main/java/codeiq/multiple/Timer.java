package codeiq.multiple;

import java.util.concurrent.TimeUnit;

/**
 *
 */
public class Timer {
    private long start;

    public void start() {
        start = System.currentTimeMillis();
    }

    public void stop() {
        long stop = System.currentTimeMillis();
        System.out.println("run time: " + TimeUnit.MILLISECONDS.toMillis(stop - start) + "(ms) [" + TimeUnit.MILLISECONDS.toSeconds(stop - start) + "(sec)]");
    }

}
