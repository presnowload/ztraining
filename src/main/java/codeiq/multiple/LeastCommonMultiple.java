package codeiq.multiple;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class LeastCommonMultiple {
    private Map<Integer, Integer> primeFactorization;

    public LeastCommonMultiple(int questionNo) {
        // 素因数分解
        // (a, b) = 50! = x^l * y^m * z^n ・・・・
        this.primeFactorization = new HashMap<Integer, Integer>();
        for (int i = 2; i <= questionNo; i++) {
            int n = i;
            n = this.getfFactorization(2, n);
            n = this.getfFactorization(3, n);
            n = this.getfFactorization(5, n);
            n = this.getfFactorization(7, n);
            n = this.getfFactorization(11, n);
            n = this.getfFactorization(13, n);
            n = this.getfFactorization(17, n);
            n = this.getfFactorization(19, n);
            n = this.getfFactorization(23, n);
            n = this.getfFactorization(29, n);
            n = this.getfFactorization(31, n);
            n = this.getfFactorization(37, n);
            n = this.getfFactorization(41, n);
            n = this.getfFactorization(43, n);
            n = this.getfFactorization(47, n);
        }
        //      System.out.println(this.primeFactorization);
    }

    private int getfFactorization(int primeNo, int n) {
        int exponentiation = 0;
        while (n % primeNo == 0) {
            exponentiation = this.primeFactorization.containsKey(primeNo) ? this.primeFactorization.get(primeNo) + 1 : 1;
            this.primeFactorization.put(primeNo, exponentiation);
            n = n / primeNo;
        }
        return n;
    }

    public long getCount() {
        // 指数の組み合わせ
        //      ex)
        //      24 = 4! = 1 * 2^3 * 3^1
        //      a = 2^l' * 3^m'
        //      b = 2^l'' * 3^m''
        //      max(l', l'') = 3, max(m', m'') = 1
        //      C(l', l'') =
        //      (0, 3), (1, 3), (2, 3), (3, 3), (3, 0), (3, 1), (3, 2)
        //      よって、C(l', l'') = 7
        //      同様に、C(m', m'') = 3
        //      求めるC=C(l', l'') * C(m', m'')となる。
        //      ここで、a≦bであるめ、
        //      C = (7 * 3 - 1) / 2 + 1 = 11

        long count = 1;
        for (Map.Entry<Integer, Integer> map : this.primeFactorization.entrySet()) {
            //          System.out.println("map.getValue = " + (map.getValue() + map.getValue() + 1));

            count = count * (map.getValue() + map.getValue() + 1);
        }
        //      System.out.println("count = " + count);
        //      System.out.println("count/2 = " + count/2);

        return count % 2 == 0 ? count / 2 : (count - 1) / 2 + 1;
    }

}
