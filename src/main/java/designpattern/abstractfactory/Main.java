package designpattern.abstractfactory;

/**
 * ▽種別
 * 　生成に関するパターン
 * 
 * ▽目的
 * 　互いに関連したり依存し合うオブジェクト群を、その具象クラスを明確にせずに生成するためのインタフェースを提供する。
 * 
 * ▽適用可能性
 * 　・システムを部品の生成、組み合わせ、表現の方法から独立にすべき場合。
 * 　・部品の集合が複数存在して、その中の1つを選んでシステムを構築する場合。
 * 　・一群の関連する部品を常に使用しなければならないように設計する場合。
 * 　・部品のクラスライブラリを提供する際に、インタフェースだけを公開して、実装は非公開にしたい場合。
 * 
 * 
 * ▽関連するパターン
 * 　Factory Method パターン、Prototype パターン: AbstractFactory クラスは、しばしば factory method を使って実装されるが、Prototype パターンを使って実装することも可能である。
 * 　Singleton パターン: ConcreteFactory オブジェクトは、しばしば、Singleton オブジェクトである。
 * 
 */
public class Main {
    public static void main(String[] args) {

    }
}
