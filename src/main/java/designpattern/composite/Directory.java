package designpattern.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Directory implements DirectoryEntry {
    //    private List<Object> list = null;
    private List<DirectoryEntry> list = null;
    private String name = null;

    public Directory(String name) {
        this.name = name;
        //        list = new ArrayList<Object>();
        list = new ArrayList<DirectoryEntry>();
    }

    public void add(DirectoryEntry file) {
        list.add(file);
    }

    public void add(Directory dir) {
        list.add(dir);
    }

    public void remove() {
        Iterator<DirectoryEntry> itr = list.iterator();
        while (itr.hasNext()) {
            ((DirectoryEntry)itr.next()).remove();
        }

        System.out.println(name + "を削除しました。");

        //        Iterator<Object> itr = list.iterator();
        //        while (itr.hasNext()) {
        //            Object obj = itr.next();
        //            if (obj instanceof File) {
        //                ((File)obj).remove();
        //            } else if (obj instanceof Directory) {
        //                ((Directory)obj).remove();
        //            } else {
        //                System.out.println("削除できません");
        //            }
        //        }
        //        System.out.println(name + "を削除しました。");
    }
}
