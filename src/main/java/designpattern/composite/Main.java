package designpattern.composite;

/**
 * ▽種別
 * 　構造に関するパターン
 * 
 * ▽目的
 * 　部分―全体階層を表現するために、オブジェクトを木構造に組み立てる。Composite パターンにより、クライアントは、個々のオブジェクトとオブジェクトを合成したものを一様に扱うことができるようになる。
 * 
 * ▽適用可能性
 * 　・オブジェクトの部分―全体階層を表現したい場合。
 * 　・クライアントが、オブジェクトを合成したものと個々のオブジェクトの違いを無視できるようにしたい場合。このパターンを用いることで、クライアントは、composite 構造内のすべてのオブジェクトを一様に扱うことができるようになる。
 * 
 * ▽関連するパターン
 * 　Chain Of Responsibility パターン: 親子関係にあるオブジェクト間のリンクは、Chain Of Responsibility パターンでしばしば使われる。
 * 　Decorator パターン: しばしば Composite パターンとともに使われる。decorator と composite を同時に使う場合、通常、これらは共通の親クラスを持つ。そのため decorator は、Add、Remove、GetChild のようなオペレーションで Component クラスのインタフェースをサポートしなければならなくなる。
 * 　Flyweight パターン: このパターンにより、component を共有できるようになる。しかし、共有されるオブジェクトは親オブジェクトを参照できなくなる。
 * 　Iterator パターン: composite を走査するために使われる。
 * 　Visitor パターン: Composite クラスや Leaf クラスに分散しているオペレーションや振る舞いを局所化する。
 * 　Faxtory Methodパターン： ポリモルフィックな iterator では、Iterator のサブクラスの中から適切なクラスをインスタンス化するために、factory method を使う。
 * 　Memento パターン：しばしば Iterator パターンとともに使われる。iterator は、iteration の状態を把握するために memento を使うことができる。この場合、iterator は memento を内部に保持する。
 * 
 * 
 */
public class Main {
    public static void main(String args[]) {
        File file1 = new File("file1");
        File file2 = new File("file2");
        File file3 = new File("file3");
        File file4 = new File("file4");

        Directory dir1 = new Directory("dir1");
        dir1.add(file1);

        Directory dir2 = new Directory("dir2");
        dir2.add(file2);
        dir2.add(file3);

        dir2.add(new SymbolicLink("link1"));
        dir2.add(new SymbolicLink("link2"));

        dir1.add(dir2);
        dir1.add(file4);

        dir1.remove();
    }

}
