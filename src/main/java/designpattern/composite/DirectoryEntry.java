package designpattern.composite;

public interface DirectoryEntry {
    public void remove();
}
