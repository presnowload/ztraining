package designpattern.adapter.inherit.homework;

/**
 * 歌手インターフェース.
 */
public interface ISinger {

    /**
     * 歌う.
     */
    public void sing();
}
