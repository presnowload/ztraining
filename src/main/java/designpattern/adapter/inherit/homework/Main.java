package designpattern.adapter.inherit.homework;

public class Main {

    public static void main(String[] args) {

        ISinger singer = new Singer();
        singer.sing();
    }
}
