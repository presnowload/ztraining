package designpattern.adapter.inherit.homework;

/**
 * 歌手実装クラス.
 */
public class Singer extends Musician implements ISinger {

    /**
     * 歌う. {@inheritDoc}
     * 
     * @see adapter.ISinger#sing()
     */
    public void sing() {
        super.singSong();
    }
}
