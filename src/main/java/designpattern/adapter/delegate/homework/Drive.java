package designpattern.adapter.delegate.homework;

/**
 * ドライブクラス
 */
public class Drive {

    public static void main(String[] args) {

        ICar car = new Ferrari();
        car.run();
        car.stop();
    }
}
