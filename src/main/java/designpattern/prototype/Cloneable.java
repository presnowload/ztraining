package designpattern.prototype;

public interface Cloneable {
    public Cloneable createClone();
}
