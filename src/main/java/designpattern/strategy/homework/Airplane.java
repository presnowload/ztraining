package designpattern.strategy.homework;

/**
 * [飛行機]予約実装クラス
 */
public class Airplane implements IReserve {

    @Override
    public void reserve(Destination destination) {

        showDialog.outputMessage(Way.AIRPLANE.getValue() + destination.getValue() + "チケット予約します");
    }

}
