package designpattern.strategy.homework;

/**
 * 移動方法によってクラスを生成
 */
public class ReservationFactory {

    public static IReserve getReservation(Way way) {
        switch (way) {
            case AIRPLANE:
                return new Airplane();
            case TRAIN:
                return new Train();
            case RENTALCAR:
                return new RentalCar();
            default:
                return new Etc();
        }
    }

}
