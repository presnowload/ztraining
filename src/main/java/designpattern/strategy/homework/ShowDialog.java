package designpattern.strategy.homework;

import javax.swing.JOptionPane;

/**
 * ダイアログ出力用クラス
 */
public class ShowDialog {

    public Way getWay() {

        int option =
            JOptionPane.showConfirmDialog(null, "飛行機乗ります？", "旅行に行くなら飛行機or電車or車？", 0, JOptionPane.QUESTION_MESSAGE);
        if (option == 0) {
            return Way.AIRPLANE;
        }
        option = JOptionPane.showConfirmDialog(null, "電車？", "旅行に行くなら飛行機or電車or車？", 0, JOptionPane.QUESTION_MESSAGE);
        if (option == 0) {
            return Way.TRAIN;
        }
        option = JOptionPane.showConfirmDialog(null, "レンタカー？", "旅行に行くなら飛行機or電車or車？", 0, JOptionPane.QUESTION_MESSAGE);
        if (option == 0) {
            return Way.RENTALCAR;
        }
        return Way.NONE;
    }

    public void outputMessage(String message) {

        JOptionPane.showMessageDialog(null, message);
    }

    public void outputMessageEtc(String message) {

        JOptionPane.showConfirmDialog(null, message, "", 0);
    }
}
