package designpattern.strategy.homework;

public enum Way {

    NONE("その他"), AIRPLANE("飛行機"), TRAIN("電車"), RENTALCAR("レンタカー");

    String value = "";

    private Way(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    //
    //    public Way toWay(String value) {
    //      if(null == value || value.isEmpty())
    //          return NONE;
    //
    //      for(Way way : values()) {
    //          if(way.name() == value) {
    //              return way;
    //          }
    //      }
    //      return NONE;
    //  }

}
