package designpattern.strategy.homework;

public class Journey {

    public static void main(String[] args) {
        ShowDialog showDialog = new ShowDialog();

        IReserve iReserve = ReservationFactory.getReservation(showDialog.getWay());
        iReserve.reserve(Destination.JAPAN);

    }

}
