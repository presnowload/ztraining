package designpattern.hello;

public interface Printer {
	public void print();
}
