package designpattern.templatemethod.homework;

public abstract class AbstractAAWriter {

    public void execute(String aaTitle, String aaString) {
        header(aaTitle);
        body(aaString);
        footer();
        write();
    }

    protected abstract void header(String aaTitle);

    protected abstract void body(String aaString);

    protected abstract void footer();

    protected abstract void write();
}
