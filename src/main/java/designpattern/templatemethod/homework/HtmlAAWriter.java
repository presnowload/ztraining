package designpattern.templatemethod.homework;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

public class HtmlAAWriter extends AbstractAAWriter {

    private final String DEFAULT_FILENAME = "AA.html";

    private StringBuffer buf = new StringBuffer();

    @Override
    protected void header(String aaTitle) {
        buf.append("<html>");
        buf.append("<head><title>" + aaTitle + "</title></head>");
        buf.append("<body>");
    }

    @Override
    protected void body(String aaString) {
        buf.append(aaString.replace(" ", "&nbsp;").replace("\n", "<br/>\n"));
    }

    @Override
    protected void footer() {
        buf.append("</body>");
        buf.append("</html>");
    }

    @Override
    protected void write() {
        PrintWriter out = null;
        try {
            Writer writer = new FileWriter(DEFAULT_FILENAME);
            out = new PrintWriter(new BufferedWriter(writer));
            out.println(buf.toString());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null)
                out.close();
        }
    }
}
