package designpattern.state.homework;

public class SunnyState implements IState {
    public void execute() {
        System.out.println("ディズニーに行きます");
    }
}