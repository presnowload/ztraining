package designpattern.state.homework;

public class Context {
    private IState state = null;

    public void setState(IState state) {
        this.state = state;
    }

    public IState getState() {
        return this.state;
    }

    public void request() {
        this.state.execute();
    }
}