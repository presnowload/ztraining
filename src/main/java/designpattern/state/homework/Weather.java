package designpattern.state.homework;

public enum Weather {

    SUNNY("晴れ"), CLOUDY("曇り"), RAINY("雨"), SNOWY("雪");

    String value = "";

    private Weather(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Weather getWeather(String str) {

        Weather[] weatherList = Weather.values();

        for (Weather weather : weatherList) {
            if (str.equals(weather.getValue())) {
                return weather;
            }
        }
        return null;
    }
}
