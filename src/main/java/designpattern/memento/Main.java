package designpattern.memento;

import java.util.HashMap;
import java.util.Map;

/**
 * ▽種別
 * 　振る舞いに関するパターン
 * 
 * ▽目的
 * 　カプセル化を破壊せずに、オブジェクトの内部状態を捉えて外面化しておき、オブジェクトを後にこの状態に戻すことができるようにする。
 * 
 * ▽適用可能性
 * 　・オブジェクトの状態（の一部）のスナップショットを、後にオブジェクトをその状態に戻すことができるように、セーブしておかなければならない場合。
 * 　・状態を得るための直接的なインタフェースが、実装の詳細を公開し、オブジェクトのカプセル化を破壊する場合。
 * 
 * ▽関連するパターン
 * 　Command パターン: command は、取り消し可能なオペレーションのために状態を保存しておくのに memento を使うことができる。
 * 　Iterator パターン: 前に述べたように、memento を iteration のために使うことができる。
 * 
 */
public class Main {
    private static Map<String, Calc.Memento> map = new HashMap<String, Calc.Memento>();

    /**
     * 計算を実行するクラス
     * @param args
     */
    public static void main(String args[]) {
        Calc calc = new Calc();
        for (int n = 1; n <= 5; n++) {
            calc.plus(n);
        }
        System.out.println(calc.getTemp());

        // ここまででSave.
        Calc.Memento memento = calc.createMemento();

        // 数日経過
        // 10までの足し算をしたい。

        Calc calc2 = new Calc();
        calc2.setMemento(memento);
        for (int n = 6; n <= 10; n++) {
            calc2.plus(n);
        }
        System.out.println(calc2.getTemp());
        map.put("10までの足し算", calc2.createMemento());
    }
}
