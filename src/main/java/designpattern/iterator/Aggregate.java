package designpattern.iterator;

public interface Aggregate {
    public designpattern.iterator.Iterator iterator();
}
