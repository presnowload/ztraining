package designpattern.iterator;

import java.util.Vector;

public class MyStudentList extends NewStudentList implements Aggregate {

    public MyStudentList() {
        super();
    }

    public MyStudentList(int studentCount) {
        if (students == null) {
            students = new Vector<Student>(studentCount);
        } else {
            students.setSize(studentCount);
        }
        //        super(studentCount);
    }

    @Override
    public designpattern.iterator.Iterator iterator() {
        return new MyStudentListIterator(this);
    }

}
