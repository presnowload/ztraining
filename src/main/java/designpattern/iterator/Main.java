package designpattern.iterator;

/**
 * ▽種別
 * 　振る舞いに関するパターン
 * 
 * ▽目的
 * 　集約オブジェクトが基にある内部表現を公開せずに、その要素に順にアクセスする方法を提供する。
 * 
 * ▽適用可能性
 * 　・集約オブジェクトの内部表現を公開せずに、その中にあるオブジェクトにアクセスしたい場合。
 * 　・集約オブジェクトに対して、複数の走査をサポートしたい場合。
 * 　・異なる集約構造の走査に対して、単一のインタフェースを提供したい（すなわち、ポリモルフィックな iteration をサポートしたい）場合。
 * 
 * ▽関連するパターン
 * 　Compositeパターン：iterator は、しばしば Composite のような再帰的な構造に対して適用される。
 * 　Faxtory Methodパターン： ポリモルフィックな iterator では、Iterator のサブクラスの中から適切なクラスをインスタンス化するために、factory method を使う。
 * 　Memento パターン：しばしば Iterator パターンとともに使われる。iterator は、iteration の状態を把握するために memento を使うことができる。この場合、iterator は memento を内部に保持する。
 * 
 * 
 */
public class Main {
    public static void main(String args[]) {
        Teacher you = new MyTeacher();
        you.createStudentList();
        you.callStudents();
    }
}
