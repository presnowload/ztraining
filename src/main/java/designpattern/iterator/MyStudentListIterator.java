package designpattern.iterator;

public class MyStudentListIterator implements designpattern.iterator.Iterator {
    private MyStudentList myStudentList;
    private int index;

    public MyStudentListIterator(MyStudentList list) {
        this.myStudentList = list;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        //return myStudentList.getLastNum() > index;
        return myStudentList.students.size() > index;
    }

    @Override
    public Object next() {
        //return myStudentList.getStudentAt(index++);
        return myStudentList.getStudentAd(index++);
    }

}
