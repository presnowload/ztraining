package sample;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dto {
    public String name = "monyu.";
    public List<String> list = new ArrayList<String>();
    public boolean b;
    public String sm;

    //    public SampleEnum getSm() {
    //        return SampleEnum.codeOf(this.sm);
    //    }
    public String getSm() {
        return this.sm;
    }

    //    public void setSm(SampleEnum sm) {
    //        this.sm = sm.getCode();
    //    }

    @Override
    public String toString() {
        return "name:" + this.name + " list.size:" + this.list.size() + " b:" + b;
    }

}
