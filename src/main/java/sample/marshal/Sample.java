package sample.marshal;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 */
public class Sample implements ISample {
    private String id;

    /**
     * marshal には、デフォルトコンストラクタが必要.
     */
    private Sample() {}

    public Sample(String id) {
        this.id = id;
    }

    /**
     * marshal 対象のプロパティには、getter / setter (public)が必要.
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
