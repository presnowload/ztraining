package sample.marshal;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.JAXB;

/**
 *
 */
public class Main {

    public static void main(String[] args) {

        ISample sample = new Sample("hoge");

        Writer xml = new StringWriter();
        JAXB.marshal(sample, xml);

        System.out.println("-------------------------------------------");
        System.out.println(xml);
        System.out.println("-------------------------------------------");

        InputStream inputStream = new ByteArrayInputStream(xml.toString().getBytes());
        ISample newSample = JAXB.unmarshal(inputStream, Sample.class);

        System.out.println("-------------------------------------------");
        System.out.println(newSample.toString());
        System.out.println("-------------------------------------------");

    }

}
