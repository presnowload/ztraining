package sample;

import java.io.Serializable;

import org.openid4java.discovery.DiscoveryInformation;

public class SerializableDto implements Serializable {

    private static final long serialVersionUID = 2815598827606585564L;

    public DiscoveryInformation discoveryInformation;
}
