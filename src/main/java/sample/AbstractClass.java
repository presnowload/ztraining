package sample;

public abstract class AbstractClass {

    public String sts;
    public String sts2;

    public AbstractClass() {
        sts = "hoge_a";
        sts2 = "hage_a";
    }

    public AbstractClass(String s1, String s2) {
        this.sts = s1;
        this.sts2 = s2;
    }

}
