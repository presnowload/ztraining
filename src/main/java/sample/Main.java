package sample;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Main {

    private static LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();

    public void setDto(Dto dto, String s) {
        dto.name = "Hello";
        dto.list.add("HOGE!");
        s = "moge";
    }

    private void mapTest(LinkedHashMap<String, String> m) {
        m = new LinkedHashMap<String, String>();
        m.put("test", "test");
    }

    private static int sum(int... nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return sum;
    }

    /**
     * デシリアライズ メソッド.
     * @param bytes Byte配列.
     * @return デシリアライズ結果.
     * @throws Exception 例外はそのままスローする.
     */
    private static <T> T deserialize(byte[] bytes) throws Exception {
        T obj = null;
        ByteArrayInputStream bin = null;
        ObjectInputStream oin = null;
        try {
            // バイト配列をストリームにする
            bin = new ByteArrayInputStream(bytes);
            // オブジェクトストリームでラップ
            oin = new ObjectInputStream(bin);
            obj = (T)oin.readObject();
        } finally {
            if (null != oin)
                oin.close();
            if (null != bin)
                bin.close();
        }
        // デシリアライズする
        return obj;
    }

    private static <T> byte[] serialize(T dto) throws Exception {
        byte[] bytes;
        ByteArrayOutputStream bout = null;
        ObjectOutputStream oout = null;
        try {
            // バイト配列ストリームを作る
            bout = new ByteArrayOutputStream();
            // バイト配列ストリームをオブジェクトストリームでラップ
            oout = new ObjectOutputStream(bout);

            // Dto オブジェクトをシリアライズ
            oout.writeObject(dto);

            bytes = bout.toByteArray();
        } finally {
            if (null != oout)
                oout.close();
            if (null != bout)
                bout.close();
        }
        return bytes;

    }

    public static String marshal(Object target) throws JAXBException {
        StringWriter writer = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(target.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(target, writer);
        return writer.toString();
    }

    /**
     * マーシャル対象のルートDtoには、@XmlRootElement アノテーションが必要ですよ。
     * Serializableとは関係ありませんよ。公開コンストラクタが無くったって問題ないんだよ。
     * 
     * @param args
     * @throws Exception
     */
    public static void marshallTest(String[] args) throws Exception {

        SrcDto srcDto = SrcDto.getInstance();

        System.out.println(srcDto.toString());

        String str = marshal(srcDto);
        System.out.println(str);
    }

    public static void print(String msgId, Object... args) {
        System.out.println(msgId + args);
    }

    public static void add(List<Integer> i) {
        List<Integer> list = new ArrayList<Integer>(i);
        list.add(1);
    }

    enum SampleEnum {
        A("aaa"),
        B("aaa"),
        C("ccc");

        public boolean is(SampleEnum t) {
            for (final SampleEnum type : values()) {
                if (type.equals(t)) {
                    return true;
                }
            }
            return false;
        }

        private String code;

        private SampleEnum(String c) {
            this.code = c;
        }

        public String getCode() {
            return this.code;
        }

        public static SampleEnum codeOf(String c) {
            if (StringUtils.isEmpty(c)) {
                return null;
            }
            for (final SampleEnum type : values()) {
                if (type.code.equals(c)) {
                    return type;
                }
            }
            return null;
        }
    }

    public static void shallowCopyTest(String... args) {

        List<Integer> i = new ArrayList<Integer>();
        i.add(1);
        System.out.println("before:" + ToStringBuilder.reflectionToString(i));
        add(i);
        System.out.println("after:" + ToStringBuilder.reflectionToString(i));

        print("ID", "A", "B");

        List<String> val1 = new ArrayList<String>();
        val1.add("12345");
        List<String> val2 = new ArrayList<String>();
        val2.addAll(val1);
        val1.add("54321");
        System.out.println(ToStringBuilder.reflectionToString(val1));
        System.out.println(ToStringBuilder.reflectionToString(val2));

    }

    public static class Dtoo1 {
        List<String> h;

        public Dtoo1() {
            h = new ArrayList<String>();
            h.add(null);
            h.add("hogehoge");

        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);

        }
    }

    public static List<String> getList() {
        System.out.println("a");
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        return list;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(1 - 0.8d);
        System.out.println(String.valueOf((int)(900 * (1 - 0.8d))));

        System.out.println(Boolean.valueOf("true"));
        System.out.println(Boolean.valueOf("1"));
        System.out.println(Boolean.valueOf("0"));
        Boolean b = new Boolean(true);

        for (String s : getList()) {
            System.out.println(s);
        }

        //        Integer integer = null;
        //        int ii = 10;
        //        if (ii <= integer) {
        //            System.out.println("true.");
        //        } else {
        //            System.out.println("false.");
        //        }
        //
        //        List<String> ll = null;//new ArrayList<String>();
        //        for (String a : ll) {
        //            System.out.println(a);
        //        }

        //        Integer ii = 1;
        //        Integer bb = null;
        //        ii += bb;
        //
        //        SampleEnum.A.is(SampleEnum.A);
        //
        //
        //        List<String> list = new ArrayList<String>();
        //        list.add("hoge");
        //        list.add("moge");
        //
        //        System.out.println(ToStringBuilder.reflectionToString(list));
        //
        //        if (list != null) {
        //            System.out.println(Arrays.toString(list.toArray()));
        //            StringBuilder values = new StringBuilder("{");
        //            for (String value : list) {
        //                values.append(value).append(",");
        //            }
        //            values.append("}");
        //            System.out.println(values.toString());
        //        }
        //
        //        //        OrdCls ordCls = EnumUtil.codeOf(OrdCls.class, "HBDBCodeConstants.DB_C_ORD_CLS_ELECTRONIC_GIFT");
        //        OrdCls ordCls = EnumUtil.codeOf(OrdCls.class, "GIFT");
        //        System.out.print(ordCls);
        //
        //        //        List h = new ArrayList();
        //        //        h.add(null);
        //        //        h.add(1);
        //        //        h.add(new Integer(5));
        //        //        h.add("hoge");
        //        //
        //        //        // null check 必要.
        //        //        for (Object object : h) {
        //        //            System.out.println(ReflectionToStringBuilder.toString(object));
        //        //        }
        //        //
        //        //        // Arrays.asList
        //        //        System.out.println(Arrays.asList(h));
        //        //
        //        //        // Reflection
        //        //        System.out.println(ToStringBuilder.reflectionToString(h));
        //        //        System.out.println(ReflectionToStringBuilder.toString(h));
        //        //
        //        //        Dtoo1 dto = new Dtoo1();
        //        //        System.out.println(dto);
        //
        //        //        for (String l : h) {
        //        //            System.out.println("hoge.");
        //        //        }
        //
        //        //        VersionList versionList = new VersionList();
        //        //        versionList.add(Version.getInstance(1, 2, 3));
        //        //        versionList.add(Version.getInstance(1, 2, 4));
        //        //        versionList.add(Version.getInstance(1, 2, 5));
        //        //        versionList.remove(0);
        //        //        for (Version element : versionList) {
        //        //            System.out.println(element);
        //        //        }
        //        //
        //        //        Version ap1 = Version.getInstance(1, 2, 3);
        //        //        Version ap2 = Version.getInstance(1, 2, 4);
        //        //        Version ap3 = Version.getInstance(1, 2, 5);
        //        //
        //        //        Map<String, Version> versionTable = new HashMap<String, Version>();
        //        //        versionTable.put("ap1", ap1);
        //        //        versionTable.put("ap2", ap2);
        //        //        versionTable.put("ap3", ap3);
        //        //
        //        //        Collection<Version> versions = versionTable.values();
        //        //        for (Iterator<Version> i = versions.iterator(); i.hasNext();) {
        //        //            System.out.println("Iterator：" + i.next());
        //        //        }

    }

    /**
     * Serializable結果に、Staticフィールドとか、メソッドは影響しませんよ。
     * ※StaticフィールドはJVM共通変数のため、復元先の値が利用されるのです。
     * 意外なことに、継承関係やインターフェース実装も影響しません。
     * マーカーインターフェースなら話もわかるのですが、この辺はシリアライズが言語的な実装外
     * VersionUIDは特殊な変数なので、影響しますよ。
     * インターフェースのSerializableを取っちゃうと、IOExceptionでちゃいますよっと。
     * どうしても除外したいフィールドがあるなら、「transient」キーワードが付けられたフィールドはSerializableの対象外になりますよ。
     * そのときは、復元先の挙動に注意してくださいね。
     */
    //    public static void main(String[] args) throws Exception {
    //
    //
    //
    //
    //        int i = 10;
    //        i <<= 3;
    //        System.out.println("i:" + i);
    //        Dto src = new Dto();
    //        src.b = true;
    //        src.sm = SampleEnum.C.code;
    //        Dto dest = new Dto();
    //        Beans.copy(src, dest).execute();
    //
    //        System.out.println(src.getClass().getName());
    //        System.out.println(src.getClass().getSimpleName());
    //
    //        System.out.println(ToStringBuilder.reflectionToString(src));
    //        System.out.println(ToStringBuilder.reflectionToString(dest));
    //
    //        //
    //        //        System.out.println(Boolean.TRUE);
    //        //        System.out.println(true);
    //        //
    //        //        SrcDto srcDto = new SrcDto();
    //        //        System.out.println(srcDto.toString());
    //        //
    //        //        DstDto destDto = new DstDto();
    //        //        System.out.println(destDto.toString());
    //        //
    //        //        Beans.copy(srcDto, destDto).execute();
    //        //
    //        //        System.out.println("Beans copy execute.");
    //        //        System.out.println(srcDto.toString());
    //        //        System.out.println(destDto.toString());
    //        //
    //        //        srcDto.s1 = "hoge";
    //        //        destDto.l1.clear();
    //        //        srcDto.date = new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
    //        //
    //        //        System.out.println("Beans edit execute.");
    //        //        System.out.println(srcDto.toString());
    //        //        System.out.println(destDto.toString());
    //        ConcreteClass dto = ConcreteClass.getInstance();//new ConcreteClass();
    //
    //        dto.sts = "_";
    //        dto.s1 = "s1s1s1";
    //        //        dto.sts2 = "__";
    //
    //        System.out.println(ToStringBuilder.reflectionToString(dto, ToStringStyle.MULTI_LINE_STYLE));
    //
    //        byte[] bytes = serialize(dto);
    //        //        {
    //        //            -84, -19, 0, 5, 115, 114, 0, 20, 115, 97, 109, 112, 108, 101, 46, 67, 111, 110, 99, 114, 101, 116, 101, 67, 108, 97, 115, 115, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 2, 76, 0, 3, 115, 116,
    //        //            115, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 83, 116, 114, 105, 110, 103, 59, 76, 0, 4, 115, 116, 115, 50, 113, 0, 126, 0, 1, 120, 112, 116, 0, 1, 95, 116, 0, 2,
    //        //            95, 95
    //        //        };//serialize(dto);
    //
    //        // バイト配列ストリームに書き込まれたバイト列を表示
    //        System.out.println(Arrays.toString(bytes));
    //
    //        ConcreteClass deserializedDto = deserialize(bytes);
    //
    //        System.out.println(ToStringBuilder.reflectionToString(deserializedDto, ToStringStyle.MULTI_LINE_STYLE));
    //
    //        //        String[] params = new String[0];
    //        //
    //        //        //        System.out.println(MessageFormat.format("At [{0}];", params));
    //        //        System.out.println(sum(null));
    //
    //    }

    /**
     * プリミティブやラッパー、EnumはImmutableですよってこと.
     */
    public static void immutableTest(String... args) {
        String s1 = "s1";
        String s2 = s1;
        s2 = "s2";
        String s3 = s2;
        s3 = "s3";
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        int i1 = 1;
        int i2 = i1;
        i2 = 2;
        int i3 = i2;
        i3 = 3;
        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);

        SampleEnum e1 = SampleEnum.A;
        SampleEnum e2 = e1;
        e2 = SampleEnum.B;
        SampleEnum e3 = e2;
        e3 = SampleEnum.C;
        System.out.println(e1);
        System.out.println(e2);
        System.out.println(e3);

        Integer ii1 = 100;
        Integer ii2 = ii1;
        ii2 = 200;
        Integer ii3 = ii2;
        ii3 = 300;
        System.out.println(ii1);
        System.out.println(ii2);
        System.out.println(ii3);

        Long l1 = 1L;
        Long l2 = l1;
        l2 = 2L;
        Long l3 = l2;
        l3 = 3l;
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
    }

    public static void main1(String[] args) throws Exception {

        //
        //        System.out.println(Boolean.TRUE);
        //        System.out.println(true);
        //
        //        SrcDto srcDto = new SrcDto();
        //        System.out.println(srcDto.toString());
        //
        //        DstDto destDto = new DstDto();
        //        System.out.println(destDto.toString());
        //
        //        Beans.copy(srcDto, destDto).execute();
        //
        //        System.out.println("Beans copy execute.");
        //        System.out.println(srcDto.toString());
        //        System.out.println(destDto.toString());
        //
        //        srcDto.s1 = "hoge";
        //        destDto.l1.clear();
        //        srcDto.date = new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
        //
        //        System.out.println("Beans edit execute.");
        //        System.out.println(srcDto.toString());
        //        System.out.println(destDto.toString());
        ConcreteClass dto = ConcreteClass.getInstance();

        dto.sts = "_";
        //        dto.sts2 = "__";

        System.out.println(ToStringBuilder.reflectionToString(dto, ToStringStyle.MULTI_LINE_STYLE));

        byte[] bytes = serialize(dto);
        //        {
        //            -84, -19, 0, 5, 115, 114, 0, 20, 115, 97, 109, 112, 108, 101, 46, 67, 111, 110, 99, 114, 101, 116, 101, 67, 108, 97, 115, 115, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 2, 76, 0, 3, 115, 116,
        //            115, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 83, 116, 114, 105, 110, 103, 59, 76, 0, 4, 115, 116, 115, 50, 113, 0, 126, 0, 1, 120, 112, 116, 0, 1, 95, 116, 0, 2,
        //            95, 95
        //        };//serialize(dto);

        // バイト配列ストリームに書き込まれたバイト列を表示
        System.out.println(Arrays.toString(bytes));
        //
        //        ConcreteClass deserializedDto = deserialize(bytes);
        //
        //        System.out.println(ToStringBuilder.reflectionToString(deserializedDto, ToStringStyle.MULTI_LINE_STYLE));

        //        String[] params = new String[0];
        //
        //        //        System.out.println(MessageFormat.format("At [{0}];", params));
        //        System.out.println(sum(null));
        //
        //        boolean is = false;
        //        System.out.println();
        //
        //        Level level = Level.OUT;
        //        System.out.println(Level.OUT.name());
        //        System.out.println(Level.OUT.ordinal());
        //        System.out.println(level.isDebug());

        //        String ss = null;
        //        if ("".equals(ss)) {
        //            System.out.println("ss:" + ss);
        //        } else {
        //            System.out.println("NULL");
        //        }
        //
        //        Main main = new Main();
        //        Dto dto = new Dto();
        //
        //        System.out.println(dto.toString());
        //
        //        String s = "ishi";
        //
        //        main.setDto(dto, s);
        //
        //        System.out.println(s);
        //        System.out.println(dto.toString());
        //
        //        List<String> list = new ArrayList<String>();
        //        if (list.isEmpty() == true) {
        //            System.out.println("TRUE!!!");
        //        }
        //        for (String val : list) {
        //            System.out.println(val);
        //        }
        //        System.out.println("END");
        //

        //		System.out.println("🔲　　");
        //		System.out.println("　🔲　");
        //		System.out.println("　　🔲");
        //
        //		Product product = new Product();
        //		System.out.println("Before:");
        //		product.print();
        //
        //		Dto dto = product.getDto();
        //		dto.name = "Hogemoge.";
        //		System.out.println("After:");
        //		product.print();

    }
}
