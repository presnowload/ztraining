package sample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VersionList implements Iterable<Version> {

    private List<Version> elements;

    public VersionList() {
        elements = new ArrayList<Version>();
    }

    public int size() {
        return elements.size();
    }

    public boolean add(Version element) {
        return elements.add(element);
    }

    public Version get(int index) {
        return elements.get(index);
    }

    public Version remove(int index) {
        return elements.remove(index);
    }

    @Override
    public Iterator<Version> iterator() {

        return new Itr();
    }

    private class Itr implements Iterator<Version> {
        int cursor = 0;

        @Override
        public boolean hasNext() {
            return cursor != VersionList.this.size();
        }

        @Override
        public Version next() {
            return VersionList.this.get(cursor++);
        }

        @Override
        public void remove() {
            VersionList.this.remove(cursor--);
        }

    }

}
