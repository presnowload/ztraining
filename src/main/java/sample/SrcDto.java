package sample;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement(name = "marshalTestDto")
public class SrcDto {
    private static final String HOGE = "HOGE.";

    public String s1;
    public String s2;
    public String s3;
    public int i1;
    public List<Dto> l1;
    public DstDto dstDto = ConcreteClass.getInstance();

    //    public Date date;

    public static SrcDto getInstance() {
        return new SrcDto("test");
    }

    private SrcDto() {}

    private SrcDto(String a) {
        s1 = HOGE;
        s2 = "ss2";
        s3 = "ss3";
        i1 = 7;
        l1 = new ArrayList<Dto>();
        for (int i = 0; i < 5; i++) {
            Dto dto = new Dto();
            dto.name = "hoge" + i;
            dto.list = new ArrayList<String>();
            for (int j = 0; j < 5; j++) {
                dto.list.add(String.valueOf(i) + String.valueOf(j));
            }
            dto.b = (i % 2 == 1);

            l1.add(dto);
        }
        //        date = new Date(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
