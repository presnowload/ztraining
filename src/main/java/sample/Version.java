package sample;

import org.apache.commons.lang3.builder.ToStringBuilder;

public final class Version {
    public final int major;
    public final int minor;
    public final int build;

    private Version(int major, int minor, int build) {
        this.major = major;
        this.minor = minor;
        this.build = build;
    }

    public static Version getInstance(int major, int minor, int build) {
        return new Version(major, minor, build);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
