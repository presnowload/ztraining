package sample;

import java.io.Serializable;

public class ConcreteClass extends DstDto implements Serializable, Cloneable, ISample {

    public String sts;
    public String sts2;
    //    transient public String sts3;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final String MSG = "messages.";

    public int getInt() {
        return i;
    }

    public String getMessage() {
        return MSG;

    }

    private ConcreteClass() {

        sts = "1";
        sts2 = "2";
    }

    public static ConcreteClass getInstance() {
        return new ConcreteClass();
    }

    private ConcreteClass(String s1, String s2) {
        sts = s1;
        sts2 = s2;
    }

    public String getSts() {
        return sts + sts2 + sts2;
    }
}
