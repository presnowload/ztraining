package sample.sqrt;

import java.math.BigDecimal;

public class Number {
    private int a;
    private static final int scale = 4;

    public Number(int a) {
        this.a = a;
    }

    public Number set(int a) {
        this.a = a;
        return this;
    }

    public boolean bingo() {
        double d = sqrt();
        int m = -1;
        for (int i = 0; i < Number.scale; i++) {
            // d:1.41421356
            d %= 1.0; // d:0.41421356
            d *= 10.0; // d:4.1421356

            int n = (int)d;
            if (-1 == m) {
                m = n;
                continue;
            }

            if (n == m)
                continue;

            return false;
        }

        if (isSquareNumber())
            return false;

        return true;
    }

    public static double sqrt(double a) {
        BigDecimal n = new BigDecimal(a);
        int d1 = 0;
        while (true) {
            if (n.compareTo(new BigDecimal(100)) < 0)
                break;

            n = n.movePointLeft(2); // /100
            d1 -= 1;
        }

        while (true) {
            if (n.compareTo(new BigDecimal(1)) >= 0)
                break;

            n = n.movePointRight(2); // *100
            d1 += 1;
        }

        int odd = 1;
        BigDecimal ans = new BigDecimal(0.0);
        for (int d2 = 0; d2 < Number.scale; d2++) {
            //        for (int d2 = 0; d2 < Integer.MAX_VALUE; d2++) {

            for (int c = 0; c < Integer.MAX_VALUE; c++) {
                if (n.compareTo(new BigDecimal(odd)) < 0) {
                    ans = ans.movePointRight(1).add(new BigDecimal(c));
                    break;
                }

                n = n.subtract(new BigDecimal(odd));
                odd += 2; // next odd.
            }

            if (d2 > Number.scale || n.compareTo(BigDecimal.ZERO) == 0) {
                ans = ans.movePointLeft(d1 + d2);
                System.out.println(a + ":" + ans.doubleValue());
                return ans.doubleValue();
            }

            n = n.movePointRight(2);
            odd = (odd - 1) * 10 + 1;
        }

        return 0;
    }

    private double sqrt() {
        //        return Math.sqrt(this.a);
        return Number.sqrt(this.a);
    }

    public boolean isSquareNumber() {
        int b = (int)Math.sqrt((double)this.a);
        return (b * b == this.a);
    }

}
