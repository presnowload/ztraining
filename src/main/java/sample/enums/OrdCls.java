package sample.enums;

import org.apache.commons.lang3.StringUtils;

import sample.enums.EnumUtil.ICodeConvirtible;

/**
 * 注文区分.
 */
public enum OrdCls implements ICodeConvirtible<String> {
    /** 注文区分 電子（ギフト） */
    ELECTRONIC_GIFT("HBDBCodeConstants.DB_C_ORD_CLS_ELECTRONIC_GIFT"),

    /** 注文区分 電子（定期） */
    ELECTRONIC_SBSC("HBDBCodeConstants.DB_C_ORD_CLS_ELECTRONIC_SBSC"),

    /** 注文区分 電子（通常） */
    ELECTRONIC_USUAL("HBDBCodeConstants.DB_C_ORD_CLS_ELECTRONIC_USUAL"),

    /** 注文区分 大口注文 */
    LARGE_ORDER("HBDBCodeConstants.DB_C_ORD_CLS_LARGE_ORDER"),

    /** 注文区分 通常通販 */
    USUAL_MAIL_ORDER("HBDBCodeConstants.DB_C_ORD_CLS_USUAL_MAIL_ORDER");

    /**
     * 値.
     */
    private String code;

    /**
     * コンストラクタ.
     * @param s 注文区分.
     */
    private OrdCls(String s) {
        this.code = s;
    }

    /**
     * code値を注文区分に変換します.
     * @param code 変換対象の文字列.
     * @return 文字列に一致する注文区分.一致が存在しない場合は、 {@link IllegalArgumentException} を返却.
     * @throws IllegalArgumentException 一致が存在しない場合.
     */
    public static OrdCls codeOf(final String code) {
        if (StringUtils.isEmpty(code)) {
            throw new IllegalArgumentException(code);
        }

        for (final OrdCls type : values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }

        throw new IllegalArgumentException(code);
    }

    /**
     * 注文区分を返却する.
     * @return 注文区分.
     */
    @Override
    public String getCode() {

        return this.code;
    }

    /**
     * 注文区分が電子か否かを判定する.
     * @return 注文区分が電子であれば、True.それ以外でFalse.
     */
    public boolean isElectronic() {
        switch (this) {
            case ELECTRONIC_GIFT:
            case ELECTRONIC_SBSC:
            case ELECTRONIC_USUAL:
                return true;
            default:
                return false;
        }
    }

    /**
     * {@link #ELECTRONIC_USUAL} か否かを判定する.
     * @return {@link #ELECTRONIC_USUAL} ならば、True.それ以外で、False.
     */
    public boolean isElectronicUsual() {
        return ELECTRONIC_USUAL.equals(this);
    }

    /**
     * {@link #ELECTRONIC_GIFT} か否かを判定する.
     * @return {@link #ELECTRONIC_GIFT} ならば、True.それ以外で、False.
     */
    public boolean isElectronicGift() {
        return ELECTRONIC_GIFT.equals(this);
    }

    /**
     * {@link #ELECTRONIC_SBSC} か否かを判定する.
     * @return {@link #ELECTRONIC_SBSC} ならば、True.それ以外で、False.
     */
    public boolean isElectronicSubscription() {
        return ELECTRONIC_SBSC.equals(this);
    }
}
