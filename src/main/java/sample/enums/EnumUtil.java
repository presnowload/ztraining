package sample.enums;

/**
 * Enumに関する汎用処理.
 */
public final class EnumUtil {

    /**
     * コード値を {@code ICodeConvirtible<T>} を実装した Enum 値に変換する.
     * @param enumClass Enum.
     * @param code 変換するコード値.
     * @return 検索値に該当するEnumを返却する.一致が存在しない場合は、IllegalArgumentException が発生する.
     */
    public static <E extends ICodeConvirtible<T>, T> E codeOf(Class<E> enumClass, final T code) {

        if (code == null) {
            throw new IllegalArgumentException("code is null.");
        }
        for (E e : enumClass.getEnumConstants()) {
            if (code.equals(e.getCode())) {
                return e;
            }
        }

        throw new IllegalArgumentException(code.toString() + "is not found.");

    }

    /**
     * コード変換可能インターフェース.
     * Enumに対しコード変換を可能とするインターフェース.
     * 
     * 
     * @param <T> コードの型.
     */
    public interface ICodeConvirtible<T> {
        /**
         * コード値を返却する.
         * @return 列挙型のコード表現値.
         */
        T getCode();
    }

}
