package fizzbuzz;

class B {
    static {
        int i = 0;
        while (i++ < 100)
            System.out.println((i % 3 < 1 ? "Fizz" : "") + (i % 5 < 1 ? "Buzz" : i % 3 < 1 ? "" : i));
    }
}