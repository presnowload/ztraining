package montyhall.yamada;

/**
 * 2回目の選択時に選ぶ封筒の番号を返す
 *
 * @author yamada
 */
public abstract class ASelectStrategy {

    /**
     * 選択した封筒の番号を返す
     *
     * @return
     */
    abstract public int getSelectEnvelop();
}
