package montyhall.yamada;

public class MontyHoleProblem {

    /**
     * ストラテジごとに試してみる.
     * @param envelopCnt 最初の封筒数
     * @param strategyNum ストラテジ番号
     * @param loop 試行回数
     */
    public void trial(int envelopCnt, int strategyNum, int loop) {

        int currentLoopCnt = 0;

        // 当たり回数
        int win = 0;

        // はずれ回数.
        int lose = 0;

        while (currentLoopCnt < loop) {
            Client client = new Client(strategyNum);
            Monty monty = new Monty();

            // 司会者があたりを入れる
            int winningEnvelopNum = monty.setWinningEnvelop(envelopCnt);

            // クライアントが封筒を選択（1回目）
            int firstSelectEnvelopNum = client.getFirstSelectEnvelopNum(envelopCnt);

            // 封筒を破る
            int tearEnvelopNum = monty.tearEnvelop(envelopCnt, firstSelectEnvelopNum, winningEnvelopNum);

            // クライアントが2回目を選択
            int secondSelectEnvelopNum =
                client.getSecondSelectEnvelopNum(envelopCnt, tearEnvelopNum, firstSelectEnvelopNum);

            if (secondSelectEnvelopNum == winningEnvelopNum) {
                win++;
            } else {
                lose++;
            }
            currentLoopCnt++;
        }

        System.out.println(StrategyFactory.getStrategyName(strategyNum) + " : [win]" + String.valueOf(win) + ",[lose]"
            + String.valueOf(lose));

    }
}
