package montyhall.yamada;

import java.util.Random;

/**
 * 司会者クラス.
 */
public class Monty {

    /**
     * 当たり封筒番号を返す
     * @param envelopCnt 最初の封筒数
     * @return 当たりの封筒番号.
     */
    public int setWinningEnvelop(int envelopCnt) {
        Random rnd = new Random();
        return rnd.nextInt(envelopCnt);
    }

    /**
     * はずれの封筒を1枚破る.
     *
     * @param envelopCnt 最初の封筒数.
     * @param firstSelectEnvelopNum クライアントが選択した封筒番号.
     * @param winningEnvelopNum 当たり封筒番号.
     * @return 破った封筒番号.
     */
    public int tearEnvelop(int envelopCnt, int firstSelectEnvelopNum, int winningEnvelopNum) {

        Random rnd = new Random();

        int tearEnvelopNum = rnd.nextInt(envelopCnt);

        // 破られた封筒番号と異なる封筒番号を選択するまで繰り返す
        while (tearEnvelopNum == firstSelectEnvelopNum || tearEnvelopNum == winningEnvelopNum) {
            tearEnvelopNum = rnd.nextInt(envelopCnt);
        }

        return tearEnvelopNum;
    }
}
