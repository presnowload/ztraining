package montyhall.yamada;

import java.util.Random;

/**
 * 初回選択時から必ず選択する封筒を変更するストラテジ
 *
 * @author yamada
 */
public class AbsoluteChangeSelectStrategy extends ASelectStrategy {

    /* 最少封筒数. */
    private final int MIN_ENVELOP_CNT = 2;

    /* 封筒数. */
    private int envelopCnt;

    /* モンティに破られた封筒番号. */
    private int tearEnvelopNum;

    /* 初回選択時の封筒番号. */
    private int firstSelectEnvelopNum;

    /**
     * @param envelopCnt 開始時の封筒数.
     * @param tearEnvelopNum 破られた封筒番号.
     * @param firstSelectEnvelopNum 初回選択時に選んだ封筒番号.
     */
    public AbsoluteChangeSelectStrategy(int envelopCnt, int tearEnvelopNum, int firstSelectEnvelopNum) {
        if (envelopCnt < MIN_ENVELOP_CNT) {
            // 後続処理で無限ループになる危険性があるので例外投げたい
        }
        this.envelopCnt = envelopCnt;
        this.tearEnvelopNum = tearEnvelopNum;
        this.firstSelectEnvelopNum = firstSelectEnvelopNum;

    }

    /**
     * 2回目にクライアントが選択する封筒番号を返す.
     */
    @Override
    public int getSelectEnvelop() {

        Random rnd = new Random();

        int selectEnvelopNum = rnd.nextInt(envelopCnt);

        // 破られた封筒番号および初回選択した封筒番号と異なる封筒番号を選択するまで繰り返す
        while (selectEnvelopNum == tearEnvelopNum || selectEnvelopNum == firstSelectEnvelopNum) {
            selectEnvelopNum = rnd.nextInt(envelopCnt);
        }

        return selectEnvelopNum;
    }

}
