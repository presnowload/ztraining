package montyhall.yamada;

import java.util.Random;

/**
 * 2回目にランダムに選択する.
 *
 * @author yamada
 */
public class RandomSelectStrategy extends ASelectStrategy {

    /* 最少封筒数. */
    private final int MIN_ENVELOP_CNT = 2;

    /* 封筒数. */
    private int envelopCnt;

    /* モンティに破られた封筒番号. */
    private int tearEnvelopNum;

    /**
     * @param envelopCnt 開始時の封筒数.
     * @param tearEnvelopNum 破られた封筒番号.
     */
    public RandomSelectStrategy(int envelopCnt, int tearEnvelopNum) {

        if (envelopCnt < MIN_ENVELOP_CNT) {
            // 後続処理で無限ループになる危険性があるので例外投げたい
        }

        this.envelopCnt = envelopCnt;
        this.tearEnvelopNum = tearEnvelopNum;
    }

    /**
     * 2回目にクライアントが選択する封筒番号を返す.
     */
    @Override
    public int getSelectEnvelop() {

        Random rnd = new Random();

        int selectEnvelopNum = rnd.nextInt(envelopCnt);

        // 破られた封筒番号と異なる封筒番号を選択するまで繰り返す
        while (selectEnvelopNum == tearEnvelopNum) {
            selectEnvelopNum = rnd.nextInt(envelopCnt);
        }

        return selectEnvelopNum;
    }
}
