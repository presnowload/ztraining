package montyhall.yamada;

public class StrategyFactory {

    /* ランダムストラテジ番号. */
    public static final int RANDOM_STRATEGY_NUM = 0;

    /* 変更なしストラテジ番号. */
    public static final int NO_CHANGE_STRATEGY_NUM = 1;

    /* 絶対変更ストラテジ番号. */
    public static final int ABSOLUTE_CHANGE_STRATEGY_NUM = 2;

    private static final String[] STRATEGY_NAMES = { "ランダムストラテジ", "変更なしストラテジ", "絶対変更ストラテジ" };

    /**
     *
     * 2回目の選択に使用するクラスを返す
     *
     * @param strategyNum ストラテジクラス番号
     * @param envelopCnt 開始時の封筒数.
     * @param tearEnvelopNum 破られた封筒番号.
     * @param firstSelectEnvelopNum 初回の選択で選んだ封筒番号
     */
    public static ASelectStrategy getSelectStrategy(int strategyNum, int envelopCnt, int tearEnvelopNum,
        int firstSelectEnvelopNum) {

        ASelectStrategy strategy = null;

        if (strategyNum == RANDOM_STRATEGY_NUM) {
            strategy = new RandomSelectStrategy(envelopCnt, tearEnvelopNum);
        } else if (strategyNum == NO_CHANGE_STRATEGY_NUM) {
            strategy = new NoChangeSelectStrategy(firstSelectEnvelopNum);
        } else if (strategyNum == ABSOLUTE_CHANGE_STRATEGY_NUM) {
            strategy = new AbsoluteChangeSelectStrategy(envelopCnt, tearEnvelopNum, firstSelectEnvelopNum);
        } else {
            // 適切なクラス番号が選択されてなかったら例外投げたい
        }

        return strategy;
    }

    /**
     * ストラテジ名を返す.
     * @param strategyNum ストラテジ番号
     * @return ストラテジ名
     */
    public static String getStrategyName(int strategyNum) {
        return STRATEGY_NAMES[strategyNum];
    }

}
