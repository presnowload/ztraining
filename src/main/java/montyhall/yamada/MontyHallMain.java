package montyhall.yamada;

public class MontyHallMain {

    public static void main(String[] args) {
        MontyHoleProblem montyHoleProblem = new MontyHoleProblem();

        montyHoleProblem.trial(3, StrategyFactory.RANDOM_STRATEGY_NUM, 10000);
    }
}
