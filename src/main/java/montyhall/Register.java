package montyhall;

import java.util.ArrayList;
import java.util.List;

public class Register {
	private List<Result> results = new ArrayList<Result>();

	private class Result {
		private Ticket montyTicket;
		private Ticket clientTicket;

		private Result() {
		}

		public Result(Ticket monty, Ticket client) {
			this.montyTicket = monty;
			this.clientTicket = client;
		}

	}

	public void add(Ticket monty, Ticket client) {
		results.add(new Result(monty, client));
	}

	public void print(boolean withDetail) {
		int i = 0;
		int montyTotal = 0, clientTotal = 0;
		for (Result result : results) {
			i++;
			int monty = result.montyTicket.isBingo() ? 1 : 0;
			int client = result.clientTicket.isBingo() ? 1 : 0;
			if (withDetail) {
				System.out.println(i + "回目(Monty,Client)：\t" + monty + "\t"
						+ client);
			}
			montyTotal += monty;
			clientTotal += client;
		}

		System.out.println("Monty :" + montyTotal);
		System.out.println("Client:" + clientTotal);

	}

}
