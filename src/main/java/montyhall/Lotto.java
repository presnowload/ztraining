package montyhall;

import java.util.ArrayList;
import java.util.List;

public class Lotto {
	private static int MAX = 3;
	private List<Ticket> tikets = new ArrayList<Ticket>();

	public Lotto() {
		for (int i = 0; i < MAX; i++) {
			tikets.add(new Ticket(i, 0 == i));
		}
	}

	public int getSize() {
		return tikets.size();
	}

	public Ticket draw(NumberAppointable user) {
		int num = user.appointNumber();
		Ticket ticket = tikets.get(num);
		tikets.remove(num);
		return ticket;
	}

}
