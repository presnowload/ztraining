package montyhall;

public class Main {

	public static void main(String[] args) {
		Register register = new Register();

		for (int i = 0; i < 50000000; i++) {
			execute(register);
		}

		register.print(false);
	}

	private static void execute(Register register) {
		// くじを作る
		Lotto lotto = new Lotto();

		// 客に引かせる
		Client client = new Client();
		Ticket clientTicket = lotto.draw(client);

		// モンティにハズレを破かせる
		Monty monty = new Monty();
		Ticket montyTicket = lotto.draw(monty);

		// 記録
		register.add(montyTicket, clientTicket);
	}
}
