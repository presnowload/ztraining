package montyhall.takagi;

/**
 * 封筒クラス.
 * @author d450131
 */
public enum Envelope {
    // アタリ
    HIT,
    // ハズレ
    MISS;

    public boolean isHit() {
        return this.equals(HIT);
    }
}
