package montyhall;

import java.util.Random;

public class Client implements NumberAppointable {
	private Ticket lot = null;

	public void setLot(Ticket lot) {
		this.lot = lot;
	}

	public Ticket getLot() {
		return lot;
	}

	@Override
	public int appointNumber() {
		return new Random().nextInt(3);
	}

}
