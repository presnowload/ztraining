package montyhall;

public class Ticket {
	private boolean isBingo;
	private int id;

	private Ticket() {
	}

	public Ticket(int id, boolean isBingo) {
		this.id = id;
		this.isBingo = isBingo;
	}

	public boolean isBingo() {
		return isBingo;
	}
}
