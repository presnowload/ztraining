package codeiq.primenumber;

import org.junit.Test;

public class MainTest {

    @Test
    public void mainTest() {
        Main.main(new String[] {
                                "2"
        });
        Main.main(new String[] {
                                "5"
        });
        Main.main(new String[] {
                                "10"
        });
        Main.main(new String[] {
                                "19"
        });
        Main.main(new String[] {
                                "54"
        });
        Main.main(new String[] {
                                "224"
        });
        Main.main(new String[] {
                                "312"
        });
        Main.main(new String[] {
                                "616"
        });
        Main.main(new String[] {
                                "888"
        });
        Main.main(new String[] {
                                "977"
        });
    }

}
