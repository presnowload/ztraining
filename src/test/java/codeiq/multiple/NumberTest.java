package codeiq.multiple;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class NumberTest {

    @Test
    public void test() {
        assertThat(new Number(-5).isPrime(), is(false));
        assertThat(new Number(0).isPrime(), is(false));
        assertThat(new Number(1).isPrime(), is(false));
        assertThat(new Number(2).isPrime(), is(true));
        assertThat(new Number(10).isPrime(), is(false));
        assertThat(new Number(15).isPrime(), is(false));
        assertThat(new Number(21).isPrime(), is(false));
        assertThat(new Number(23).isPrime(), is(true));
        assertThat(new Number(97).isPrime(), is(true));
        assertThat(new Number(74717).isPrime(), is(true));
    }

}
